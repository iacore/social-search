import render from "https://esm.sh/preact-render-to-string@6.4.2";

if (import.meta.main) {
  Deno.serve((req) => {
    const App = <div class="foo" >content</div>;;
    console.log(App)

    return new Response(render(App));
  });
}
